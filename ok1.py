
import sqlite3
import flask
from flask import request, jsonify
import argparse
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token, verify_jwt_in_request, get_jwt_claims,
    get_jwt_identity
)

import datetime
from functools import wraps
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from sqlalchemy import Column, Boolean, DateTime, Integer

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
app.config['JSON_SORT_KEYS'] = False
jwt = JWTManager(app)

# Truyền đường dẫn database từ dòng lệnh , ex: python user.py "data/user.db"
parser = argparse.ArgumentParser(description='path file')
parser.add_argument('path', help='path file', nargs='?', default='/home/genis/user.db')
data = parser.parse_args()
path_db = data.path

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + str(path_db)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(30), unique=True, nullable=False)
    password = db.Column(db.String(30), nullable=False)
    role = db.Column(db.String(20), server_default="minh")
    delete_at = db.Column(DateTime(timezone=True))
    deleted = db.Column(db.Boolean(), default=False)

db.create_all()
# Check user/pass trước khi call API
@app.route('/users1/<id>', methods=['DELETE'])
def delete_user(id):

    user = User.query.get(id)
    if user == None:
        return "ID not match"
    if user.deleted:
        return "User deleted"
    user.deleted = True
    user.delete_at = func.now()
    db.session.commit()
    return jsonify({'deleted_user': user.user}), 200

@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
    conn_sql = sqlite3.connect(path_db)
    user_pass = conn_sql.execute("SELECT role from users where user='" + str(username)+"' AND password='"+str(password)+"\'")
    for check in user_pass:
            expires = datetime.timedelta(days=365)
            identity = {
                'user': username,
                'role': check[0]
            }
            access_token = create_access_token(identity=identity, expires_delta=expires)
            return jsonify(access_token=access_token), 200
    return jsonify({"msg": "Invalid username or password"}), 401

# Check role của user (access nếu role == admin)
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        current_user = get_jwt_identity()

        if current_user['role'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper

#List all users dưới dạng json (required role = admin)
@app.route('/users', methods=['GET'])
@admin_required
def get_users():
    try:
        conn_sql = sqlite3.connect(path_db)
        cursor = conn_sql.execute("SELECT * from users")
        users = []
        for row in cursor:
            a = {"ID": row[0], "user": row[1], "password": row[2], "age": row[3], "role": row[4]}
            users.append(a)
        conn_sql.close()
        if len(users) == 0:
            return jsonify("Data empty"), 404
    except :
        return jsonify("Unable to open database file"), 500
    return jsonify(users), 200

# Get one user by id from the path
@app.route("/user/<id>", methods=['GET'])
# @jwt_required
def get_user_by_id_in_path(id):
    try:
        with open('data/users.json','r') as f:
            data = json.load(f)
        i = 0
        while data[i]['id'] != int(id):
            i += 1
            if i == len(data):
                return 'ID not found', 404
        return data[i]
    except FileNotFoundError:
        return 'Data not found on server', 500

# Add new user
@app.route('/user', methods=['POST'])
@jwt_required
def post_users():
    try:
        new_user = {
            "user": request.json["user"],
            "password": request.json["password"],
            "age": request.json["age"]
        }
    except :
        return jsonify("Body format json and only include user, password and age "), 400
    try:
        new_users = User(user=new_user["user"], password=new_user["password"])
        db.session.add(new_users)
        db.session.commit()
    except sqlite3.IntegrityError:
        return("Username already exists. Please try another one "), 500
    except:
        return jsonify("Unable to open database file"), 500

    return jsonify({"new user": new_user}), 201


# Update user
@app.route('/user', methods=['PUT'])
@jwt_required
def put_users():
    try:
        new_data={
            "name": flask.request.json['name'],
            "age": flask.request.json['age'],
            "id": flask.request.json['id']
        }
    except:
        return 'New data must be in the right format: {"name":"new_name","age":new_age,"ID": ID_number}', 400
    try:
        with open('data/users.json','r') as f:
            data = json.load(f)
        i=0
        while data[i]['id'] != new_data['id']:
            i+=1
            if i == len(data):
                return 'User is not exist!', 404
        data[i] = new_data
        with open('data/users.json', 'w') as f:
            json.dump(data, f)
        return 'Update user successfully!'
    except FileNotFoundError:
        return 'Data not found on server', 500

# Delete user by id
@app.route('/user/<id>', methods=['DELETE'])
# @jwt_required
def delete_users(id):
    try:
        conn_sql = sqlite3.connect(path_db)
        check_id = conn_sql.execute("SELECT ID from users")
        for i in check_id:
            if int(i[0]) == int(id):
                user = conn_sql.execute("SELECT  user from users where ID =" + str(id))
                conn_sql.execute("DELETE from users where ID =" + str(id))
                conn_sql.commit()
                user_del = [u[0] for u in user]
                conn_sql.close()
                return jsonify({"del":{"id": id, "user": user_del[0]}}), 200
        return "ID not match", 400
    except :
        return ("Unable to open database file"), 500


app.run(debug=True, host='0.0.0.0')
# curl -H "Content-Type: application/json"  -XPOST -d '{"name":"Adam","age":14,"id":1}' http://localhost:5000/user
# curl -H "Content-Type: application/json"  -XPUT -d '[{"name":"Eva","age":14,"id":1}]' http://localhost:5000/user
# curl -X DELETE http://localhost:5000/user/1